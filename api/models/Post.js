

module.exports = {

  attributes: {
  	
  	titulo: {
  		type: 'string',
  		unique: true, 
      required: true
  	},

    author: function(req, res){
      req.body.author = req.session.user;
    },

  	contenido: {
  		type: 'string',
  		unique: true, 
      required: true
  	}

  }

};
